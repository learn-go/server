package main

import (
	"fmt"
	"net"

	"bitbucket.org/learn-go/server/myserver"
)

func main() {
	var clientIndex int = 0                                // UN-USED: to track number of clients
	addr, _ := net.ResolveTCPAddr("tcp", "localhost:8888") // Register network address for TCP server

	fmt.Printf("Listenning at localhost:8888...\n")
	listener, _ := net.ListenTCP("tcp", addr)

	for {
		conn, _ := listener.AcceptTCP()
		clientIndex++
		fmt.Printf("\nClient connected\n")
		go myserver.HandleConnection(conn, clientIndex)
	}
}
