package myserver

import (
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
	"strings"
)

// HandleConnection is called when receives a connection accepted
func HandleConnection(conn *net.TCPConn, clientIndex int) {
	buffer := make([]byte, 10)
	var cmdRecv string

	for {
		c, e := conn.Read(buffer)
		if e != nil {
			if e == io.EOF {
				fmt.Printf("Client disconnected!!\n")
				break
			} else {
				break
			}
		}

		if c > 0 && buffer[c-1] == 10 {
			cmdRecv = cmdRecv + string(buffer[0:c-1])
			cmd := strings.Split(cmdRecv, " ")
			size, _ := strconv.ParseInt(cmd[2], 10, 64)

			// PROTOCOL
			switch cmd[0] {
			case "SEND":
				log.Println("Receive command: ", cmdRecv)
				//BUG: go handleSend(conn, clientIndex, cmd[1], size)
				HandleSend(conn, clientIndex, cmd[1], size)
			}

		} else {
			cmdRecv = cmdRecv + string(buffer[0:c])
			// fmt.Printf("%s", buffer[0:c])
		}
	}

}
