package myserver

import (
	"io"
	"log"
	"net"
	"os"
	"strconv"

	"bitbucket.org/learn-go/file-utils/myutils"
)

// HandleSend is used for SEND cmd
func HandleSend(conn *net.TCPConn, clientIndex int, filePath string, size int64) {
	if myutils.FileExists("./files/" + filePath) {
		info, _ := os.Stat("./files/" + filePath)

		// If size of file on server = expectation size of file, response STOP to client
		if info.Size() == size {
			message := "STOP" + " " + filePath + " " + strconv.FormatInt(info.Size(), 10) + "\n"
			conn.Write([]byte(message))
			log.Printf("File %s existed, file size %d!!\n", filePath, info.Size())
		}

		// If size of file on server < expectation size of file, response START to client
		if info.Size() < size {
			message := "START" + " " + filePath + " " + strconv.FormatInt(info.Size(), 10) + "\n"
			conn.Write([]byte(message))
			log.Printf("START: File %s is transfering again!!\n", filePath)

			saveFile(conn, filePath)
		}

	} else {
		// If file is not existed, response START to client
		size := strconv.FormatInt(0, 10)
		message := "START" + " " + filePath + " " + size + "\n"
		conn.Write([]byte(message))
		saveFile(conn, filePath)

		log.Printf("START: File %s is transfering!!\n", filePath)

	}
}

// Used when START receive file from client
func saveFile(conn *net.TCPConn, filePath string) {
	buffer := make([]byte, 10)

	fp, _ := os.OpenFile("./files/"+filePath, os.O_CREATE|os.O_WRONLY, 0777)

	defer fp.Close()

	for {
		c, e := conn.Read(buffer)
		if e != nil {
			if e == io.EOF {
				fp.Write(buffer[0:c])
				break
			} else {
				break
			}
		} else {
			fp.Write(buffer[0:c])
		}

	}
}
