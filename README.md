# Run following command, server will be started at port 8888

go run main.go

# Simple protocol

Client request format: SEND filename filesize

Server check if filename is existed:

    - File is existed and have the same size with size expectation, server response

        STOP filename filesize

    - File is existed and have the smaller size than size expectation, server response

        START filename filesize

    - File is not existed
    
        START filename filesize

